
#include "parser.hpp"

Parser::Parser(const std::string file)
{
    if(!file_exists(file))
        throw invalid_argument("File don't exist");
    try
    {
        fs = new std::fstream();
        fs->open (file, std::fstream::in);
    }
    catch(...)
    {
        throw runtime_error("Could not open the file stream");
    }
    length = get_length(fs);
}

Parser::Parser(std::fstream *fs)
{
    this->fs = fs;
    length = get_length(fs);
    if(length == 0)
        throw invalid_argument("File doesn't exist");
}
/*
 * Reads HEADER_SIZE bytes which is passed to the Header class.
 * Moves the position of the file reader PACKAGE_SIZE bytes after
 * each read. 
 */
void Parser::parse()
{
    char char_header[HEADER_SIZE];// = new char[HEADER_SIZE];
    int last_cont = 0;
    for(int i = 0; i < length/PACKAGE_SIZE; i++)
    {
        fs->seekg(i*PACKAGE_SIZE);
        fs->read(char_header,HEADER_SIZE);  
        Header header = Header(char_header);
        if(header.sync != SYNC_BYTE)
            num_sync_errors++;

        bool cont_success = is_continuity(header.adaptation,header.continuity,last_cont);
        store_information(header.PID,header.scramble, cont_success);

    }
    print_information();

}

int Parser::get_length(fstream *fs)
{
    int length = fs->tellg();
    fs->seekg( 0, std::ios::end );
    length = fs->tellg() - length;
    fs->seekg(0); //Return the postion to the beginning of the file
    return length;
}
/*
 * store_information handles the statistics for the different PID:s  
 * Contains a map<pair<int,int>,int> which used as
 * map<pair<PID,scramble>,count>
 */
void Parser::store_information(const int PID, const int scramble, const bool cont_success)
{
    pair<int,int> tmp_pair = make_pair(PID,scramble);
    map<pair<int,int>,fields>::iterator it;
    it = PID_map.find(tmp_pair);
    if(it != PID_map.end())
    {
        it->second.count++; //Add one to the counter
        if(!cont_success)
            it->second.cont_errors++;
    }
    else
    {
        struct fields new_field;
        if(!cont_success)
            new_field.cont_errors++;
        PID_map[tmp_pair] = new_field; //New pair found, add it and set the counter to 1
    }
}
bool Parser::is_continuity(const int adaptation,const int current_cont, int &last_cont)
{
    bool res = false;
    if(adaptation == 0b01 || adaptation == 0b11)
    {
        //Compare with the current_cont value 
        //Special case: Wraps around 15->0
        if(last_cont == 15)
        {
            res = current_cont == 0;
        }
        else
        {
            res =  current_cont == last_cont + 1;
            
        }
        last_cont = current_cont;
    }
    else
    {
        //The counter should be the same as last header
        res =  current_cont == last_cont;
    }
    return res;

}       

void Parser::print_information()
{
    //Layout of the map: <<PID,scramble>,fields>
    //it->first.first = PID, it->first.second = scramble
    int total = length/PACKAGE_SIZE;
    cout << "Number of sync errors: " << num_sync_errors << endl;
    cout << "PID\t" << "COUNT (%)\t" << "Scrambled (%)\t" << "Cont errors (%)\t" << endl;
    for (std::map<pair<int,int>,fields>::iterator it=PID_map.begin(); it!=PID_map.end(); ++it)
        cout<< std::setprecision(3)
            << it->first.first << '\t' << it->second.count 
            << "  (" << (100*it->second.count/(double) total) << "%)\t" 
            << (it->first.second ? "yes" : "no") << "\t\t" //Scramble value > 0 counts as scrambled
            << it->second.cont_errors << "  (" <<  (100*it->second.cont_errors/(double) it->second.count ) << "%)"
            << endl;

}
std::map<std::pair<int,int>,fields> Parser::get_map()
{
    return PID_map;
}

//From stackoverflow :)
bool Parser::file_exists(const string filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}
