#include <iostream>
#include <bitset>
#include "parser.hpp"
using namespace std;


void print_help()
{
    cout << "Usage: ./parser.o filename" << endl
         << "Example: ./parser.o Streams/03-13.ts" << endl;
}
int main(int argc,  char** argv)
{
    if(argc == 2)
    {
        if((string) argv[1] == "--h")
            print_help();
        else
        {
            Parser parser = Parser(argv[1]);
            parser.parse();    
        }
        
    }
    else
    {
        cout<< "Wrong input format" << endl;
        print_help();
    }

    return 0;
}



/* TODO
* Analysera data
* Kolla efter continuity_counter - fel. Verkar inte vara superlätt. 
* Gör sanity-checks på input
*/