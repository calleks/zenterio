As a Software Engineer you will be working in Zenterio's Development department.
Your daily duties will include designing and implementing features, reviewing
source code and debugging software, providing feedback to other developers.
With this task we would like you to try an example of a daily assignment for a
Software Engineer.

== Assignment ==
A mpeg2 transport stream consists of a sequence of transport stream packets.
The transport stream packets are defined in Section 2.4.3 in ISO/IEC 13818-1.

Your task is to write a transport stream analyzer, that, given a transport
stream shows some basic statistics of the transport stream. It should be easy
to reuse parser in other applications. As we are working in Linux it is also
a preferred development environment for the task. You are encouraged to use STL
when needed.

It shall at least print out (not in a priority order):
- What PIDs are used
- How many packets of each PID are there (% of total)
- Detect sync errors
- Detect continuity counter errors
- Detect whether a PID is scrambled (note that TS packets with same PID could
be scrambled and unscrambled in the same TS)

Optional info:
- Detect whether a PID carries PES or PSI data
- Bit rate (bits/s) of the whole stream and each PID

Below you can find an example of the output:
    Number of sync errors: 43

    --PID-- --Count (%)-- --Scrambled (%)-- --Cont. errors (%)-- (opt) --Type-- --Bit rate--
    0x00	1000 (2%)		no (0%) 				0 (0%)	              PSI 		xxx bits/s
    0x100	100000 (97%)	yes (98%)				5000 (5%)             PES		xxx bits/s
    ...

== Data ==
Together with the assignment description you also get documentation and transport streams for analysis.

Documentation includes:
* "Docs/iso13818-1.pdf" - An international standard describing Transport stream syntax and semantics
* "Docs/Mpeg2TS_A3.pdf" - Mpeg2 Transport streams poster
* "Docs/links.txt" - useful links

== Delivery ==
The delivery deadline is as agreed upon between yourself and the Zenterio
representative. If you, for whatever reason, need to change that deadline,
please inform your contact person.

Your delivery shall consist of source code and a Makefile that can build the
application, it shall build on a Linux machine. If you have any comments, ideas
or suggestions, please include them in the delivery.

Your application will be tested against the sample streams as well as other
streams.
