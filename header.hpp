#ifndef HEADER_HPP
#define HEADER_HPP

#define PACKAGE_SIZE 188 //Byte size of the TS package
#define HEADER_SIZE 4 //Byte size of the TS header
#define SYNC_BYTE 0x47 //Sync byte for error correction

#include <iostream>

/*
 * Header class which handles each TS header package. 
 * Implements some sanity checks for each package, e.g. the sync byte  
 */

class Header
{
public:
    int sync,PID,scramble,adaptation,continuity;
    bool error,start,prio;
    bool sync_error = false;

    /*
     * Constructor for Header. 
     * Sets the variables according to the specification in the
     * ISO/IEC 13818-1 where all the bits are specified. 
     */
    Header(const char char_header[HEADER_SIZE]);
    void print();

private:
    bool get_bit(const unsigned char byte, const int position);
};
#endif