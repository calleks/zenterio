#ifndef PARSER_HPP
#define PARSER_HPP

#include <map>
#include <fstream>
#include "header.hpp"
#include <sys/stat.h>
#include <iomanip> 
using namespace std;


/*
 * Parser class which handles the whole parsing by creating Header objects.
 * Collects the data from each package required by the assignment
 * and prints it as specified.
 * Implements two constructors, one with the string to the file and one
 * where a fstream is taken as input 
 *
 * It doesn't save all the information in the headers but only the ones needed for the assignment 
 */
struct fields
{
    int count = 1;
    int cont_errors = 0;
};
class Parser
{
private: 
    std::fstream *fs;
    int length;
    int num_sync_errors = 0;
    std::map<std::pair<int,int>,fields> PID_map;
public:

    Parser(const std::string file);
    Parser(std::fstream *fs);
    /*
     * Reads HEADER_SIZE bytes which is passed to the Header class.
     * Moves the position of the file reader PACKAGE_SIZE bytes after
     * each read. 
     */
    void parse();
    /*
     * Returns the PID_map. Unused in the project is implemented for 
     * usage in production
     */
    std::map<std::pair<int,int>,fields> get_map();
    private:
    int get_length(std::fstream *fs);
    /*
     * store_information handles the statistics for the different PID:s  
     * Contains a map<pair<int,int>,int> which used as
     * map<pair<PID,scramble>,fields>
     */
    void store_information(const int PID, const int scramble, const bool cont_error);
    /*
     * is_continuity checks if the continuity_counter in the TS header
     * 1) Should be incremented and 2) is incremented.
     * The interpretation of the ISO 13818-1 is that if
     * there is payload (0b01 or 0b11) the counter should be incremented.
     */
    bool is_continuity(const int adaptation,const int continuity, int &last_count);
    void print_information();
    bool file_exists(const string filename);
};
#endif
