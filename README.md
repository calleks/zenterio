Build with

make

How to use:
./parser.o filename

Example:

./parser.o Streams/03-13.ts

Output: 
Outputs statistics about the mpeg2-stream, an example is shown below.

Number of sync errors: 0
PID     COUNT (%)       Scrambled (%)
0       113  (2%)       no
17      23  (0%)        no
256     4297  (90%)     no
257     224  (4%)       no
3839    113  (2%)       no
