#include "header.hpp"

Header::Header(const char char_header[HEADER_SIZE])
{
    sync = char_header[0];
    error = get_bit(char_header[1],7);
    start = get_bit(char_header[1],6);
    prio = get_bit(char_header[1],5);
    PID = (((int) char_header[1] & 0b00011111) << 8) + char_header[2]; //5 bits from char_header[1] and all from char_header[2]
    scramble = (char_header[3] & 0b11000000) >> 6;
    adaptation = (char_header[3] & 0b00110000) >> 4;
    continuity = char_header[3] & 0b00001111;
}
void Header::print()
{
    std::cout<< "sync " << sync << std::endl
             << "error " << error << std::endl
             << "start " << start << std::endl
             << "prio " << prio << std::endl
             << "PID " << PID << std::endl
             << "scramble " << scramble << std::endl
             << "adaptation " << adaptation << std::endl
             << "continuity " << continuity << std::endl;    
}

bool Header::get_bit(const unsigned char byte, const int position) // position in range 0-7
{
    return (byte >> position) & 0x1;
}

